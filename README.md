CODELABS

HTML5 - CSS3 - JQUERY - STYLUS - BOOTSTRAP

@Labscommunity  @Bootmack  @Drakho  @iTheCode


*******************************

Tutor: @Bootmack

*******************************

HTML5:


HTML5 es el más probable siguiente estándar en la web, es la ultima version de HTML(Lenguaje marcado de hipertexto).

Si bien todas las web que puedes visitar están creadas en base al antiguo HTML, esta versión está limitada sólo a imágenes y texto, la versión HMTL5 está más enfocada a mejorar y cambiar los paradigmas del desarrollo web.

Pues si bien HTML5, aun no es un estándar en la web, trae nuevas etiquetas que hacen que tu web esté mejor estructurada.

--------

 * ESTRUCTURA:

<!DOCTYPE html>
<html lang="es">
<head>
   <meta charset="utf-8"/>
   <title>CODELABS</title>
</head>
<body>

</body>
</html>

Que de nuevo nos trae??


<!DOCTYPE html>

La diferencia entre otros Doctype, son:

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">


En Html, existen las famosas etiquetas. Como se diferencian las etiquetas?
De la siguiente manera: 

<NombreEtiqueta> contenido dentro de la etiqueta</NombreEtiqueta>

Una etiqueta es un componente necesario para estructurar bien un web-site.
Hay nuevas etiquetas que implementa Html5, entre las cuales están:

- Video
- Audio
- Header
- Section
- Article
- Footer
- Entre otras...


Novedades: 


> No son necesarias las comillas.

> Podemos usar Expresiones regulares.

> Nuevas etiquetas como Audio y Video.

> Estructura para nuestro trabajo y obtener una web mas semantica.

> Contenido Editable desde navegador.

> Llamada a scripts Asincronicamente.

> Nuevos inputs que realizan validaciones.

No es necesario cerrar con /, las etiquetas vacias como <br> <hr> <img src="imagen.jpg">.

--------

 * NAV

En html5 ya tenemos destinada una etiqueta para la zona de navegacion, como lo hacemos simplemente usando la etiqueda predeterminada Nav, esta nos permite agrupar nuestros menus o todo tipo de navegacion que tenga el portal, la etiqueda es la siguiente <nav>, con su respectiva etiqueta de cierre, claro, </nav>.

--------

* HEADER - FOOTER

Como en todo portal tenemos una cabecera y un pie de pagina, html5 nos da 2 nuevas etiquetas para realizar esto! las cuales son Header y Footer, las podemos usar donde nosotros vayamos a tener un pie de pagina y una cabecera, como lo podriamos usar?

un ejemplo con la etiqueta ARTICLE Y SECTION:
<article>
	<section>
		<header></header>
		<div class=content> </div> <!--No son necesarias comillas-->
		<footer></footer>
	</section>

	<section>
		<header></header>
		<div class=content> </div> <!--No son necesarias comillas-->
		<footer></footer>
	</section>
</article>

Muy facil de entender verdad?, estas novedades nos trae Html5 con esto podemos lograr una web mas semantica.

--------

 * ARTICLE - SECTION

Si bien nosotros podemos estructurar nuestro portal con divisiones, ahora html5 nos permite optimizar nuestro trabajo al maximo. Nos da Article y Section para estructurar nuestro contenido, por ejemplo el contenido de un foro y/o una publicacion nueva en caso de un blog.

Ojo: Section y Div no significan lo mismo, si bien podriamos usar div para hacer lo mismo, lo recomendable es usar section. Por ejemplo si tenemos un contenido que tiene imagenes y parrafos de un mismo autor, eso seria un section. 

Pero si tenemos imagenes variadas de varios autores podriamos usar Div.


<article>
	<section>

	</section> <!--end section 1 --> 

	<section>

	</section> <!--end section 2 --> 
</article>

--------

 * NUEVOS INPUT:


Html5, nos permite usar nuevos input y/o cajas de texto, para usarlo con nuestros formularios...En estos momentos mostraremos los nuevos input.


<input type=“text”>

<input type=“search”>

<input type=“datetime” >

<input type=“email” >

<input type="number" min="0" max="50">

Estos son algunos de los mas interesantes, ya que hay muchos mas.

Html5 tambien nos trae nuevos atributos para nuestros inputs, como los siguiente:

- pattern: Es usado para hacer validaciones en nuestros inputs.

- placeholder: Nos permite poner un texto de ayuda al usuario en el input.

- autofocus: Selecciona automáticamente el Input al cargar la pagina, usado para buscadores.

- required: Hace que el campo de texto sea requerido, si no es requerido no ejecutará          
                      ninguna acción.


Como dijimos antes Html5 nos permite hacer validaciones, para hacer una validacion podemos usar una expresion regular, quedaria de la siguiente manera:


<input type=“email” name="twitter"  pattern="^[A-Za-z0-9_]{1,15}$" required>

Usamos pattern, como ya lo dijimos hace unas lineas arriba.

--------

 * CONTENIDO EDITABLE EN HTML5

Html5 nos permite hacer una caja de texto, o cualquiera de nuestros tag's  un contenido editable, como lo hacemos? de la siguiente manera: contenteditable="true"

Solo tendriamos que poner ese atributo en nuestro tag, aca un ejemplo:

<hgroup>
	<h1 contenteditable="true"> Nosotros Somos Codelabs </h1>
</hgroup>

--------

 * VIDEO

Con Html5 podemos incluir video en nuestra web, como lo hacemos? con la etiqueta Video.

Esta etiqueta es muy facil de usar simplemente tenemos que pasar los parametros de nuestro video y esta lo reproducira, tenemos que validar primeramente si el formato es visible en los principales navegadores.

<video src="video.mp4" controls width="360" height="240">

</video>

Con el parametro controls, le decimos que queremos que se vean los controles del video.

Que pasa si quieremos poner diferentes formatos de video??

Es muy simple, ya que la etiqueta es multi formato y podemos ponerlo de la siguiente manera:

<source src="video.ogv" type="video/ogg" />
    
<source src="video.mp4" type="video/mp4" />    


Ahora, hay un problema ya que en IE8 - IE9 - SAFARI, presentan problemas con la etiqueta video.

--------

 * AUDIO

Asi como nos permite usar video, tambien podemos usar audio de la siguiente manera:

<audio src="cancion.mp3">

</audio>

Al igual que Video, a la etiqueta audio le pasamos parametros:


Autoplay: Su mismo nombre lo dice.

Loop: Nos permite hacer la reproduccion en bucle.

Control: Nos permite ver o mostrar los controles.

--------

 * COMPATIBILIDAD

 Html5 no tiene aun la compatibilidad en todos los navegadores, puesto que aun no es un estandar, siempre hay cambios y por eso es mejor usar librerias o toolkits para mejorar el rendimiento y para logar que se vea en la mayoria de navegadores.

 De por si presentara problemas con IE(internet explorer), pero sin duda su version 9 almenos nos permite trabajar con un poco de normalidad.